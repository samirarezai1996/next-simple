import React from 'react';
import Link from "next/link";
import Image from 'next/image'
const Index = (props) => {
    console.log(props)
    return (
        <div>
            <h2 className="text-center">products</h2>
            <div className="container ">
                <div className="row">
                    {props.products.map((r,i)=>  <div key={i} className="col-12 col-md-4  my-2">
                        <div className="card h-100 p-3">
                            <div>
                                <img
                                    src={r.image}
                                    alt="Picture of the author"
                                    className="img-fluid"
                                    height={100}
                                />
                            </div>
                            <Link href={`/products/${r?.id}` }>
                                {r?.title}
                            </Link></div></div>)}
                </div>
            </div>
        </div>
    );
};

export const getStaticProps = async () => {
    const res = await fetch('https://fakestoreapi.com/products')
    const products = await res.json()
    return {
        props: {
            products,
        },
    }
}

export default Index;