import React from 'react';
import {useRouter} from "next/router";
import Head from 'next/head'
const Product = (props) => {
    const {query: { id },} = useRouter();
    const {product} = props;
    return (
        <div className="card  m-4 p-4">
            <Head>
                <title>{product.title}</title>
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                <meta name="description" content={product.description} />
            </Head>
            <h2>{product.title}</h2>
            <div className="text-center">
                <img src={product.image}
                     className="img-fluid"
                     width={300}
                     alt=""/>
            </div>


            <p className="pt-4">
                {product.description}
            </p>
        </div>
    );
};

export async function getStaticPaths() {
    // Call an external API endpoint to get posts
    const res = await fetch('https://fakestoreapi.com/products')
    const products = await res.json()

    // Get the paths we want to pre-render based on posts
    const paths = products.map((product) => ({
        params: { id: product.id.toString() },
    }))

    // We'll pre-render only these paths at build time.
    // { fallback: false } means other routes should 404.
    return { paths, fallback: false }
}
export async function getStaticProps({ params }) {
    // params contains the post `id`.
    // If the route is like /posts/1, then params.id is 1
    const res = await fetch(`https://fakestoreapi.com/products/${params.id}`)
    const product = await res.json()

    // Pass post data to the page via props
    return { props: { product } }
}
export default Product;